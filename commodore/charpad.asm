
#importonce
#import "c64.asm"


/*
    A Library To Work With CharPad

    The basic idea behind this library is to make
    it easier for the developer to work with
    map data that is exported from CharPad.

    The Map data itself is usually accompanied by a
    numbe of supplementatal files that contains 
    information related to the charater set, the tiles,
    the tile attributes, etc.

    The library will help setup this information at
    compile time, so that at runtime all we need to 
    do is tell it what map we want to load into
    memory.

*/

/*
    CharPad Library
*/

// has the charpad library been initialized?
.var __charpad_init = false

.namespace Charpad {

    .label COLORING_METHOD_GLOBAL = 0
    .label COLORING_METHOD_TILE = 1
    .label COLORING_METHOD_CHARACTER = 2

    .label DRAW_METHOD_TILE = 0
    .label DRAW_METHOD_CHAR = 1
    

    // vic bank and address
    .var charpadVicBank = 0
    .var charpadVicBankAddr = $c000

    // vic screen location and address
    .var charpadScreenLoc = 0
    .var charpadScreenAddr = $c000

    // vic charset location and address
    .var charpadCharLoc = 0
    .var charpadCharAddr = $f000

    // charpad data
    .var charpadCharData = -1
    .var charpadCharAttrData = -1
    .var charpadTileData = -1
    .var charpadTileAttrData = -1
    .var charpadMapData = -1

    // charpad data starting and supporting addresses
    .var charpadDataAddr = $8000
    .var charpadCharAttrAddr = -1
    .var charpadTileAddr = -1
    .var charpadTileAttrAddr = -1
    .var charpadMapAddr = -1

    // charpad map properties
    .var charpadTiles = 0 // 0 means that it uses characters only (tiles are disabled)
    .var charpadTileWidth = 0
    .var charpadTileHeight = 0
    .var charpadMaps = 0
    .var charpadMapWidth = 0
    .var charpadMapHeight = 0
    .var charpadMultiColor = true // assume multicolor
    .var charpadColoringMethod = COLORING_METHOD_TILE // by default use character coloring
    .var charpadDrawMethod = DRAW_METHOD_TILE // by default use tiles

    // calculated area of map and tiles`
    .var charpadTileArea = 0
    .var charpadMapArea = 0

    .var charpadColor = $d800

    .var charpadTileLookupAddrHi = 0
    .var charpadTileLookupAddrLo = 0

    /*
        Initialize the Charpad Library
    */
    .macro @CharpadInit(vic_bank, screen_memory, charset_memory, tiles, tile_width, tile_height, maps, map_width, map_height, coloring_method, draw_method, char_attr_addr, tile_addr, tile_attr_addr, map_addr) {

        .eval charpadVicBank = vic_bank
        .eval charpadScreenLoc = screen_memory
        .eval charpadCharLoc = charset_memory

        .eval charpadCharAttrAddr = char_attr_addr
        .eval charpadTileAddr = tile_addr 
        .eval charpadTileAttrAddr = tile_attr_addr
        .eval charpadMapAddr = map_addr 

        .eval charpadTiles = tiles
        .eval charpadTileWidth = tile_width
        .eval charpadTileHeight = tile_height
        .eval charpadTileArea = tile_width * tile_height

        .eval charpadMaps = maps
        .eval charpadMapWidth = map_width
        .eval charpadMapHeight = map_height
        .eval charpadMapArea = map_width * map_height

        .eval charpadColoringMethod = coloring_method 
        .eval charpadDrawMethod = draw_method

        .eval __charpad_init = true

    }

    /*
        Set Map Colors
    */
    .macro @CharpadSetColors (background_color, multi_color1, multi_color2, border_color) {
        lda #background_color 
        sta VIC2.BGCOL0
        lda #border_color 
        sta VIC2.EXTCOL 
        lda #multi_color1 
        sta VIC2.BGCOL1
        lda #multi_color2 
        sta VIC2.BGCOL2 
    }
    

    /*
        Draw a Map
    */    
    .pseudocommand @charpad_draw map {

        .if(map.getType() == AT_IMMEDIATE) {
            lda #map.getValue()
        } else {
            lda map.getValue()
        }
        jsr _charpad_draw
    }

    /*
        Draw a Map
    */
    .macro @CharpadDraw(map) {
        charpad_draw map
    }
  

    /*
        Load CharPad Macros
    */
    .macro @CharpadLoad() {

        .if(__charpad_init) {

            __build__()

            #if DEBUG
                CharpadShowInfo()
            #endif

        }

    }

    .macro __build__() {

        * = * "Charpad Library"

        #if DEBUG
            .print "CharpadDraw(_charpad_draw) subroutine @ " + AddressOf(*)
        #endif

        @_charpad_draw:

            VicSetBank(charpadVicBank)
            VicSetScreenMemory(charpadScreenLoc)
            VicSetCharacterMemory(charpadCharLoc)

            .if (charpadMultiColor) {
                VicSetMode(VIC2.MODE_MULTICOLOR)
            }

            lda #$00
            sta _row
            sta _col

            // set top of screen ram
            lda #<charpadScreenAddr
            sta scr + 1
            lda #>charpadScreenAddr
            sta scr + 2

            // set top of color ram
            lda #<charpadColor
            sta col + 1
            lda #>charpadColor
            sta col + 2

            // set top of map
            lda #<charpadMapAddr
            sta tile + 1
            lda #>charpadMapAddr
            sta tile + 2

        loop_row:

            lda #$00
            sta _col

        loop_col:

            ldx #$00
            ldy #$00

            .if(charpadTiles > 0) {
                lda #$00
                sta tile_lookup + 2
            }

        tile:

            lda $b00b

            .if (charpadColoringMethod == COLORING_METHOD_TILE) {
                sta _temp
                tax 
                lda charpadTileAttrAddr, x
                sta _tile_color
                lda _temp
            }

            .if(charpadTiles > 0) {

                tax 
                lda _tile_lookup_lo, x
                sta tile_lookup + 1
                lda _tile_lookup_hi, x 
                sta tile_lookup + 2
            }

        tile_lookup:

            .if(charpadTiles > 0) {

                // draw tile
                lda $b00b, y
                ldx _tile_screen_locations, y

            }

        scr:

            sta $b00b, x

            // set color
            .if (charpadColoringMethod == COLORING_METHOD_CHARACTER) {
                tax 
                lda charpadCharAttrAddr, x
            }

            .if (charpadColoringMethod == COLORING_METHOD_TILE) {
                lda _tile_color
            }

            ldx _tile_screen_locations, y

        col:

            sta $b00b, x

            iny
            cpy #(charpadTileWidth * charpadTileHeight) // <- 16 characters for each tile
            bne tile_lookup

            // advance next col
            clc 
            lda tile + 1
            adc #$01 
            sta tile + 1
            lda tile + 2
            adc #$00
            sta tile + 2

            // advance next screen and color
            clc
            lda scr + 1
            adc #charpadTileWidth // <- width of tile?
            sta scr + 1
            sta col + 1
            bcc !+
            inc scr + 2
            inc col + 2
        !:

            inc _col
            ldx _col 
            cpx #charpadMapWidth
            beq !+
            jmp loop_col
        !:

            // advance next row
            clc 
            lda scr + 1
            adc #((charpadTileHeight-1) * 40) // <- height of tile?
            sta scr + 1
            sta col + 1
            bcc !+
            inc scr + 2
            inc col + 2
        !:
        
            clc
            inc _row
            ldx _row
            cpx #charpadMapHeight 
            beq !+
            jmp loop_row
        !:

            rts 

        // tile character locations
        _tile_screen_locations:
        
            .for(var r = 0; r < charpadTileHeight; r++) {
                .fill charpadTileWidth, (r * 40) + i
            }

        // tile lookup table hi
        .eval charpadTileLookupAddrHi = *
        _tile_lookup_hi:
            .if (charpadTiles > 0) {
                .for(var r = 0; r < charpadTiles; r++) {
                    .byte >charpadTileAddr + (r * (charpadTileWidth * charpadTileHeight))
                }
            }

        .eval charpadTileLookupAddrLo = *
        _tile_lookup_lo:
            .if (charpadTiles > 0) {
                .for(var r = 0; r < charpadTiles; r++) {
                    .byte <charpadTileAddr + (r * (charpadTileWidth * charpadTileHeight))
                }
            }

        _temp:
            .byte
        _row:
            .byte 0
        _col:
            .byte 0
        _tile_color:
            .byte 0

    }

    .function @CharpadGetCharsetLocation() {
        .return charpadCharData
    }

    .function @CharpadGetColoringMethodString() {
        .if (charpadColoringMethod == COLORING_METHOD_GLOBAL) .return "Global"
        .if (charpadColoringMethod == COLORING_METHOD_TILE) .return "Per Tile"
        .if (charpadColoringMethod == COLORING_METHOD_CHARACTER) .return "Per Character"
        .return "* unknown *"
    }

    .function @CharpadGetDrawingMethodString() {
        .if (charpadDrawMethod == DRAW_METHOD_TILE) .return "Tile"
        .if (charpadDrawMethod == DRAW_METHOD_CHAR) .return "Character"
        .return "* unknown *"
    }

    /*
        Shows Configuration Information
    */
    .macro @CharpadShowInfo() {
        .print("Charpad Information:")
        .print("  Character Set @ " + AddressOf(charpadCharAddr))
        .print("  Character Attributes @ " + AddressOf(charpadCharAttrAddr))
        .print("  Tiles @ " + AddressOf(charpadTileAddr))
        .print("  Tile Attributes @ " + AddressOf(charpadTileAttrAddr))
        .print("  Maps @ " + AddressOf(charpadMapAddr))
        .print("  Vic Bank is " + ToHex(charpadVicBank))
        .print("  Vic Screen is " + ToHex(charpadScreenLoc))
        .print("  Vic Charset @ " + ToHex(charpadCharLoc))
        .print("  Vic Screen @ " + ToHex(charpadScreenAddr))
        .print("  Drawing Method is " + CharpadGetDrawingMethodString())
        .print("  Multicolor Mode is " + charpadMultiColor)
        .print("  Coloring Method is " + CharpadGetColoringMethodString())
        .print("  Tile Lookup Table Hi @ " + ToHex(charpadTileLookupAddrHi))
        .print("  Tile Lookup Table Lo @ " + ToHex(charpadTileLookupAddrLo))
    }

}
