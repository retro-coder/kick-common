/*
    A Common Set of Macros and Functions for CBM Development
*/

#importonce

.var NONE = 0
.var NULL = ""
.var __NONE__ = NONE
.var __NULL__ = NULL

.namespace Common {

    /*
        Save all Registers to the Stack
    */
    .macro @PushRegisters() {
        { pha; txa; pha; tya; pha; }
    }

    /*
        Save the CPU Status to the Stack
    */
    .macro @PushStatus() {
        { php; }
    }

    /*
        Restore all Registers from the Stack
    */
    .macro @PullRegisters() {
        { pla; tay; pla; tax; pla; }
    }

    /*
        Restore the CPU Status from the Stack
    */
    .macro @PullStatus() {
        { plp; }
    }

    /*
        Safely Call a Subroutine at Address by saving the Register and CPU Status first.
    */
    .macro @CallSafe(address) {
        PushStatus()
        PushRegisters()
        Call(address)
        PullRegisters()
        PullStatus()
    }

    /*
        Call Subroutine without Saving Register or CPU Status
    */
    .macro @Call(address) {
        jsr address
    }

    /*
        Jump to Address without Saving Register or CPU Status
    */
    .macro @Jump(address) {
        jmp address
    }

    //  Returns the Address of a Symbol in Hex and Decimal
    .function @AddressOf(symbol) {
        .return "$" + toHexString(symbol, 4) + " (" + symbol + ")"
    }

    //  Returns the Hex Format of a Value
    .function @ToHex(value) {
        .return "$" + toHexString(value, 2)
    }

    // Returns the Decimal Format of a Value
    .function @ToDecimal(value) {
        .return value
    }

    // Returns the Binary Format of a Value
    .function @ToBinary(value) {
        .return toBinaryString(value)
    }

    // Determines if a given Value is within a Range of Values
    .function @RangeTest(value, min_value, max_value) {
        .if (value.getValue() < min_value || value.getValue() > max_value)
            .error "value is out of range " + toIntString(min_value) + " to " + toIntString(max_value)
    }

    .function @CheckParameter(param, value, min_value, max_value) {
        .if (value < min_value || value > max_value)
            .error param + "(" + toIntString(value) + ") out of range " + toIntString(min_value) + " to " + toIntString(max_value)
    }

}
