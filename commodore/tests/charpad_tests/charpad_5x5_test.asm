/*
    This will test using the Charpad library
    to build a 5x5 single screen map.

*/

#define DEBUG

#importonce
#import "..\..\c64.asm"
#import "..\..\charpad.asm"

BasicUpstart2(start)

* = $4000 "MAIN"

start:

    sei
    
    lda #$7f
    sta $dc0d
    sta $dd0d

    // bank out basic and kernal rom
    lda $01
    and #%11111000
    ora #%00000101
    sta $01

    // Wadawadoo
    CharpadInit(3, 0, 6, 5, 5, 5, 1, 8, 5, 1, 0, charpad_attribs, charpad_tiles, charpad_tile_attr, charpad_maps)
    CharpadSetColors(BLACK, BROWN, GRAY, BLUE)


    CharpadDraw(0)


end:

    cli 
    jmp *

    CharpadLoad()


* = $f000 "Charset"
charpad_charset:
    .import binary "maps\Hexagone (5x5) - Chars.bin"
    
* = $8000 "Charpad Data"

charpad_tiles:
    .import binary "maps\Hexagone (5x5) - Tiles.bin"
        
charpad_tile_attr:
    .import binary "maps\Hexagone (5x5) - TileAttribs.bin"
        
charpad_attribs:
    .import binary "maps\Wadawadoo (4x4) - CharAttribs.bin"
    
charpad_maps:
    .import binary "maps\Hexagone (5x5) - Map (8x5).bin"
        
