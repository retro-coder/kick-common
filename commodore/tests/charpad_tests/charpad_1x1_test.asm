/*
    This will test using the Charpad library
    to build a 2x2 single screen map.

*/

#define DEBUG

#importonce
#import "..\..\c64.asm"
#import "..\..\charpad.asm"

BasicUpstart2(start)

* = $4000 "MAIN"

start:

    sei

    lda #$7f
    sta $dc0d
    sta $dd0d

    // bank out basic and kernal rom
    lda $01
    and #%11111000
    ora #%00000101
    sta $01

    // Initialize the Charpad Library
    CharpadInit(3, 0, 6, 0, 1, 1, 1, 40, 25, 2, 0, charset_attr_addr, __NONE__, __NONE__, map_addr)
    CharpadSetColors(BLACK, GRAY, WHITE, BLUE)

    // Draw a Map
    CharpadDraw(0)

end:

    cli 
    jmp * 

    CharpadLoad()


* = $f000 "Charset"
charpad_charset:
    .import binary "maps\Blob (1x1) - Chars.bin"
    
* = $8000 "Charpad Data"
    
charset_attr_addr:
    .import binary "maps\Blob (1x1) - CharAttribs.bin"

map_addr:
    .import binary "maps\Blob (1x1) - Map (40x200).bin"
