/*
    This will test using the Charpad library
    to build a 1x1 single screen map.

*/

#define DEBUG

#importonce
#import "..\..\c64.asm"
#import "..\..\charpad.asm"


BasicUpstart2(start)

* = $4000 "MAIN"

start:

    sei

    lda #$7f
    sta $dc0d
    sta $dd0d

    // bank out basic and kernal rom
    lda $01
    and #%11111000
    ora #%00000101
    sta $01


    // Initialize the Charpad Library
    CharpadInit(3, 0, 6, 41, 2, 2, 1, 20, 12, 2, 0, charset_attr_addr, tile_addr, __NONE__, map_addr)
    CharpadSetColors(BROWN, LIGHT_GRAY, GRAY, BLUE)

    // Draw a Map
    charpad_draw #$00

end:

    cli 
    jmp * 

    CharpadLoad()

    .var charData = LoadBinary("maps\Untitled (2x2) - Chars.bin")
    .var charAttrData = LoadBinary("maps\Untitled (2x2) - CharAttribs.bin")
    .var tileData = LoadBinary("maps\Untitled (2x2) - Tiles.bin")
    .var tileAttrData = __NONE__ 
    .var mapData = LoadBinary("maps\Untitled (2x2) - Map (20x12).bin")

* = $f000 "Charset"

charset_addr:
    .fill charData.getSize(), charData.get(i)

* = $8000 "Charpad Data"

charset_attr_addr:
    .fill charAttrData.getSize(), charAttrData.get(i)

tile_addr:
    .fill tileData.getSize(), tileData.get(i)

map_addr:
    .fill mapData.getSize(), mapData.get(i)