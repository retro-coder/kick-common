/*
    This will test using the Charpad library
    to build a 4x4 single screen map.

*/

#define DEBUG

#importonce
#import "..\..\c64.asm"
#import "..\..\charpad.asm"


BasicUpstart2(start)

* = $4000 "MAIN"

start:

    sei
    
    lda #$7f
    sta $dc0d
    sta $dd0d

    // bank out basic and kernal rom
    lda $01
    and #%11111000
    ora #%00000101
    sta $01

    // Wadawadoo
    CharpadInit(3, 0, 6, 9, 4, 4, 1, 10, 6, 2, 0, charset_attr_addr, tile_addr, __NONE__, map_addr)
    CharpadSetColors(BROWN, BLACK, WHITE, BLUE)

    charpad_draw 0


end:

    cli 
    jmp * 

    CharpadLoad()

* = $f000 "Charset"
charset_addr:
    .import binary "maps\Wadawadoo (4x4) - Chars.bin"
    
* = $8000 "Charpad Data"
charset_attr_addr:
    .import binary "maps\Wadawadoo (4x4) - CharAttribs.bin"
    
tile_addr:
    .import binary "maps\Wadawadoo (4x4) - Tiles.bin"

map_addr:
    .import binary "maps\Wadawadoo (4x4) - Map (10x6).bin"
    
    