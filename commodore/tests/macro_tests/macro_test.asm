
.var data = LoadBinary("Hexagone (5x5) - Chars.bin")
.var dataloc = $8000


.namespace test {
    
    .var @test1 = data
    .var @test2 = dataloc

    .macro @test(something) {
        .if(test1.getSize() > 0) .print(test1.getSize())
        * = test2 "TEST1"
        .fill test1.getSize(), test1.get(i)
    }

}

test(test1)

.printnow(data)
.printnow(dataloc)
.printnow(test1)
.printnow(test2)
