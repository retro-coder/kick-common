#importonce

.namespace VIC2 {

    .label COLOR_BLACK = BLACK;
    .label COLOR_WHITE = WHITE;

    .label MODE_HIRES = 0
    .label MODE_MULTICOLOR = 1

    .var vicBank = 0
    .var vicCharMem = 0
    .var vicScreenMem = 0

    /*
        Sprite Horizontal and Vertical Position Registers
    */
    .label SHVPR    = $d000
    /*
        Sprite 0 Horizontal Register
    */
    .label SP0X     = $d000     
    /*
        Sprite 0 Vertical Register
    */
    .label SP0Y     = $d001
    .label SP1X     = $d002     
    .label SP1Y     = $d003
    .label SP2X     = $d004     
    .label SP2Y     = $d005
    .label SP3X     = $d006     
    .label SP3Y     = $d007
    .label SP4X     = $d008     
    .label SP4Y     = $d009
    .label SP5X     = $d00a     
    .label SP5Y     = $d00b
    .label SP6X     = $d00c     
    .label SP6Y     = $d00d
    .label SP7X     = $d00e     
    .label SP7Y     = $d00f

    /*
        Most Significant Bits (MSB) of Sprites 0-7 Horizonal Position

        - bit 0: Sprite 0 MSB
        - bit 1: Sprite 1 MSB
        - bit 2: Sprite 2 MSB
        - bit 3: Sprite 3 MSB
        - bit 4: Sprite 4 MSB
        - bit 5: Sprite 5 MSB
        - bit 6: Sprite 6 MSB
        - bit 7: Sprite 7 MSB

        Setting one of these bits to 1 adds 256 to the horizontal position of the corresponding sprite. Resetting one of these bits to 0 restricts the horizontal position of the corresponding sprite to a value of 255 or less
    */
    .label MSGIX    = $d010

    /*
        Vertical Fine Scrolling
    */
    .label SCROLY   = $d011

    /*
        Read Current Raster Line
    */
    .label RASTER   = $d012

    /*
        Light Pen Registers
    */
    .label LPENX    = $d013
    .label LPENY    = $d014

    /*
        Sprite Enable Registers

        Bit 0: Enable Sprite 0
    */
    .label SPENA    = $d015     

    /*
        Horizontal Fine Scrolling and Control Register
    */
    .label SCROLX   = $d016

    /*
        Sprite Vertical Expansion Register
    */
    .label YXPAND   = $d017

    /*
        VIC-II Chip Memory Control Register
    */
    .label VMCSB    = $d018 

    /*
        VIC Interrupt Flag Register
    */
    .label VICIRQ   = $d019

    /*
        IRQ Mast Register
    */
    .label IRQMSK   = $d01a

    /*
        Sprite to Foreground Display Priority
    */
    .label SPBGPR   = $d01b

    /*
        Sprite Multicolor Registers
    */
    .label SPMC     = $d01c

    /*
        Sprite Horizontal Expansion Register
    */
    .label XXPAND   = $d01d

    /*
        Sprite to Sprite Collision Register
    */
    .label SPSPCL   = $d01e

    /*
        Sprite to Foreground Collision Register
    */
    .label SPBGCL   = $d01f 

    /*
        Border Color Register
    */
    .label EXTCOL   = $d020

    /*
        Background Color 0
    */
    .label BGCOL0   = $d021

    /*
        Background Color 1
    */
    .label BGCOL1   = $d022

    /*
        Background Color 2
    */
    .label BGCOL2   = $d023 

    /*
        Background Color 3
    */
    .label BGCOL3   = $d024 

    /*
        Sprite Multicolor Register 0
    */
    .label SPMC0    = $d025

    /*
        Sprite Multicolor Register 1
    */
    .label SPMC1    = $d026 

    /*
        Sprite Color Registers
    */
    .label SPCOL    = $d027 
    .label SP0COL   = $d027
    .label SP1COL   = $d028
    .label SP2COL   = $d028
    .label SP3COL   = $d02a
    .label SP4COL   = $d02b
    .label SP5COL   = $d02c
    .label SP6COL   = $d02d
    .label SP7COL   = $d02e

    /*
        Increase Border Color
    */
    .macro @VicIncreaseBorderColor() {
        inc EXTCOL
    }

    /*
        Decrease Border Color
    */
    .macro @VicDecreaseBorderColor() {
        inc EXTCOL
    }

    /*
        Set Background Color
    */
    .macro @VicSetBackgroundColor(color) {
        lda #color
        sta BGCOL0
    }

    /**
        Set Border Color
    */
    .macro @VicSetBorderColor(color) {
        lda #color 
        sta EXTCOL
    }

    /*
        Set Vic Bank

        - 0 = $0000-$3fff (default)
        - 1 = $4000-$7fff
        - 2 = $8000-$bfff
        - 3 = $c000-$ffff
    */
    .macro @VicSetBank(bank) {
        .eval vicBank = bank
        .var _val = %00000011                       //  bank 0 $0000-$3fff (default)
        .if(vicBank == 1) .eval _val = %00000010    //  bank 1 $4000-$7fff
        .if(vicBank == 2) .eval _val = %00000001    //  bank 2 $8000-$bfff
        .if(vicBank == 3) .eval _val = %00000000    //  bank 3 $c000-$ffff
        lda $dd00
        and #%11111100
        ora #_val
        sta $dd00
    }

    /*
        Return Vic Bank
    */
    .function @VicGetBank() {
        .return vicBank
    }

    /*
        Set Vic Character Map Memory Location

        - 0 = $0000
        - 1 = $0800
        - 2 = $1000
        - 3 = $1800
        - 4 = $2000
        - 5 = $2800
        - 6 = $3000
        - 7 = $3800

    */
    .macro @VicSetCharacterMemory(charmem) {
        .eval vicCharMem = charmem
        .var _char = 0
        .if(vicCharMem == 00) .eval _char = %00000000   //  $0000
        .if(vicCharMem == 01) .eval _char = %00000010   //  $0800
        .if(vicCharMem == 02) .eval _char = %00000100   //  $1000
        .if(vicCharMem == 03) .eval _char = %00000110   //  $1800
        .if(vicCharMem == 04) .eval _char = %00001000   //  $2000
        .if(vicCharMem == 05) .eval _char = %00001010   //  $2800
        .if(vicCharMem == 06) .eval _char = %00001100   //  $3000
        .if(vicCharMem == 07) .eval _char = %00001110   //  $3800
        lda $d018
        and #%11110001
        ora #_char
        sta $d018
    }

    /*
        Set Screen Mode

        - 0 = Hi Resolution Text Mode
        - 1 = Multicolor Text Mode
    */

    .macro @VicSetMode(mode) {

        lda $d016
        and #%11101111

        .if (mode == MODE_HIRES) {
            ora #%00000000
        }

        .if (mode == MODE_MULTICOLOR) {
            ora #%00010000
        }

        sta $d016
    }

    .function  @VicGetCharacterMemory() {
        .return vicCharMem
    }

    /*
        Set Screen Memory Location

        - 00 = $0000
        - 01 = $0400 (default)
        - 02 = $0800
        - 03 = $0c00
        - 04 = $1000
        - 05 = $1400
        - 06 = $1800
        - 07 = $1c00
        - 08 = $2000
        - 09 = $2400
        - 10 = $2800
        - 11 = $2c00
        - 12 = $3000
        - 13 = $3400
        - 14 = $3800
        - 15 = $3c00

    */
    .macro @VicSetScreenMemory(screenmem) {
        .eval vicScreenMem =screenmem
        .var _screen = 0
        .if (vicScreenMem == 00) .eval _screen = %00000000    //  $0000
        .if (vicScreenMem == 01) .eval _screen = %00010000    //  $0400
        .if (vicScreenMem == 02) .eval _screen = %00100000    //  $0800
        .if (vicScreenMem == 03) .eval _screen = %00110000    //  $0c00
        .if (vicScreenMem == 04) .eval _screen = %01000000    //  $1000
        .if (vicScreenMem == 05) .eval _screen = %01010000    //  $1400
        .if (vicScreenMem == 06) .eval _screen = %01100000    //  $1800
        .if (vicScreenMem == 07) .eval _screen = %01110000    //  $1c00
        .if (vicScreenMem == 08) .eval _screen = %10000000    //  $2000
        .if (vicScreenMem == 09) .eval _screen = %10010000    //  $2400
        .if (vicScreenMem == 10) .eval _screen = %10100000    //  $2800
        .if (vicScreenMem == 11) .eval _screen = %10110000    //  $2c00
        .if (vicScreenMem == 12) .eval _screen = %11000000    //  $3000
        .if (vicScreenMem == 13) .eval _screen = %11010000    //  $3400
        .if (vicScreenMem == 14) .eval _screen = %11100000    //  $3800
        .if (vicScreenMem == 15) .eval _screen = %11110000    //  $3c00
        lda $d018
        and #%00001111
        ora #_screen
        sta $d018
    }

    .function @VicGetScreenMemory() {
        .return vicScreenMem
    }


    .macro @VicClearScreen(value) {

        .var _address = vicBank * $4000 + (vicScreenMem * $0400) - 1
        lda #value
        ldx #$c8
    !:
        sta _address + $c8 * 0, x
        sta _address + $c8 * 1, x
        sta _address + $c8 * 2, x
        sta _address + $c8 * 3, x
        sta _address + $c8 * 4, x

        dex 
        bne !-
        
    }
    



}