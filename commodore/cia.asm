
.namespace CIA {

    // Data Port A
    .label PRA = $00
    // Data Port B
    .label PRB = $01
    // Data Direction Port A
    .label DDRA = $02
    // Data Direction Port B
    .label DDRB = $03
    // Timera A Lo
    .label TALO = $04
    // Timer A Hi
    .label TAHI = $05
    // Timer B Lo
    .label TBLO = $06
    // Timer B Hi
    .label TBHI = $07
    // Real Time Clock Tenths of Second
    .label TOD10THS = $08
    // Real Time Clock Seconds
    .label TODSEC = $09
    // Real Time Clock Minutes
    .label TODMIN = $0a
    // Real Time Clock Hours
    .label TODHR = $0b
    // Serial Shift Register
    .label SDR = $0c
    // Interrupt Control & Status
    .label ICR = $0d
    // Control Timer A
    .label CRA = $0e
    // Control Timer B
    .label CRB = $0f
}

.namespace CIA1 {

    /*
        Monitoring/control of the 8 data lines of Port A. The lines are used for multiple purposes:
        - Read/Write: Bit 0..7 keyboard matrix columns
        - Read: Joystick Port 2: Bit 0..3 Direction (Left/Right/Up/Down), Bit 4 Fire button. 0 = activated.
        - Read: Lightpen: Bit 4 (as fire button), connected also with "/LP" (Pin 9) of the VIC
        - Read: Paddles: Bit 2..3 Fire buttons, Bit 6..7 Switch control port 1 (%01=Paddles A) or 2 (%10=Paddles B)        
    */
    .label PRA = $dc00 
    .label PRB = $dc01 
}