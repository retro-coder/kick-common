/*
    copyright 2021 SpockerDotNet LLC
    released under MIT License.
*/

#importonce

#import "c64.asm"

.namespace Sprite {

    .macro @SpriteReset(sprite) {
        SpriteResetPosition(sprite)
    }


    .macro @SpriteResetAll() {
        .for (var i = 0; i < 8; i++) {
            SpriteResetPosition(i)
        }

        SpriteResetMulticolorMode()
        SpriteResetMsb()
        SpriteDisableAll()
    }

    /*
        Set Sprite Color
    */
    .macro @SpriteSetColor(sprite, color) {
        lda #color
        sta VIC2.SPCOL + sprite
    }

    .macro @SpriteSetMulticolor(color0, color1) {
        lda #color0 
        sta VIC2.SPMC0
        lda #color1
        sta VIC2.SPMC1
    }

    .macro @SpriteSetPosition(sprite, x, y) {
        .var offset = sprite * 2
        lda #x + 24
        sta VIC2.SP0X + offset
        lda #y + 50
        sta VIC2.SP0Y + offset
    }

    .macro @SpriteResetPosition(sprite) {
        .var offset = sprite * 2
        lda #$00
        sta VIC2.SP0X + offset
        sta VIC2.SP0Y + offset
    }
    
    //  Reset the Sprite MSB Register
    .macro @SpriteResetMsb() {
        lda #$00
        sta VIC2.MSGIX        
    }

    //  Reset Sprite Multicolor Mode Register
    .macro @SpriteResetMulticolorMode() {
        lda #$00
        sta VIC2.SPMC
    }

    //  Disable All Sprites
    .macro @SpriteDisableAll() {
        lda #$00
        sta VIC2.SPENA
    }

    //  Enable All Sprites
    .macro @SpriteEnableAll() {
        lda #$ff
        sta VIC2.SPENA
    }

    .macro @SpriteSetMsb(sprite, value) {
        .if (value != true) {
            .if(sprite == 0) clear_bit0 VIC2.MSGIX
            .if(sprite == 1) clear_bit1 VIC2.MSGIX
            .if(sprite == 2) clear_bit2 VIC2.MSGIX
            .if(sprite == 3) clear_bit3 VIC2.MSGIX
            .if(sprite == 4) clear_bit4 VIC2.MSGIX
            .if(sprite == 5) clear_bit5 VIC2.MSGIX
            .if(sprite == 6) clear_bit6 VIC2.MSGIX
            .if(sprite == 7) clear_bit7 VIC2.MSGIX
        } else {
            .if(sprite == 0) set_bit0 VIC2.MSGIX
            .if(sprite == 1) set_bit1 VIC2.MSGIX
            .if(sprite == 2) set_bit2 VIC2.MSGIX
            .if(sprite == 3) set_bit3 VIC2.MSGIX
            .if(sprite == 4) set_bit4 VIC2.MSGIX
            .if(sprite == 5) set_bit5 VIC2.MSGIX
            .if(sprite == 6) set_bit6 VIC2.MSGIX
            .if(sprite == 7) set_bit7 VIC2.MSGIX
        }
    }

    .macro @SpriteSetEnable(sprite, value) {
        .if (value != true) {
            .if(sprite == 0) clear_bit0 VIC2.SPENA
            .if(sprite == 1) clear_bit1 VIC2.SPENA
            .if(sprite == 2) clear_bit2 VIC2.SPENA
            .if(sprite == 3) clear_bit3 VIC2.SPENA
            .if(sprite == 4) clear_bit4 VIC2.SPENA
            .if(sprite == 5) clear_bit5 VIC2.SPENA
            .if(sprite == 6) clear_bit6 VIC2.SPENA
            .if(sprite == 7) clear_bit7 VIC2.SPENA
        } else {
            .if(sprite == 0) set_bit0 VIC2.SPENA
            .if(sprite == 1) set_bit1 VIC2.SPENA
            .if(sprite == 2) set_bit2 VIC2.SPENA
            .if(sprite == 3) set_bit3 VIC2.SPENA
            .if(sprite == 4) set_bit4 VIC2.SPENA
            .if(sprite == 5) set_bit5 VIC2.SPENA
            .if(sprite == 6) set_bit6 VIC2.SPENA
            .if(sprite == 7) set_bit7 VIC2.SPENA
        }
    }

    .macro @SpriteResetPositionAll() {
        .for(var i = 0; i <= 7; i++) {
            SpriteResetPosition(i)
        }
    }

    .macro @SpriteSetMultiColorMode(sprite, value) {
        .if (value != true) {
            .if(sprite == 0) clear_bit0 VIC2.SPMC
            .if(sprite == 1) clear_bit1 VIC2.SPMC
            .if(sprite == 2) clear_bit2 VIC2.SPMC
            .if(sprite == 3) clear_bit3 VIC2.SPMC
            .if(sprite == 4) clear_bit4 VIC2.SPMC
            .if(sprite == 5) clear_bit5 VIC2.SPMC
            .if(sprite == 6) clear_bit6 VIC2.SPMC
            .if(sprite == 7) clear_bit7 VIC2.SPMC
        } else {
            .if(sprite == 0) set_bit0 VIC2.SPMC
            .if(sprite == 1) set_bit1 VIC2.SPMC
            .if(sprite == 2) set_bit2 VIC2.SPMC
            .if(sprite == 3) set_bit3 VIC2.SPMC
            .if(sprite == 4) set_bit4 VIC2.SPMC
            .if(sprite == 5) set_bit5 VIC2.SPMC
            .if(sprite == 6) set_bit6 VIC2.SPMC
            .if(sprite == 7) set_bit7 VIC2.SPMC
        }
    }

    .macro @SpriteSetMultiColorModeAll(value) {
        .for(var i = 0; i <= 7; i++) {
            SpriteSetMultiColorMode(i, value)
        }
    }

    .macro @SpriteSetPointer(sprite, pointer) {
        .var _bank = VicGetBank()
        .var _screen = VicGetScreenMemory()
        .var _address = (_bank * $4000) + (_screen * $0400) + $03f8 + sprite
        lda #pointer
        sta _address
    }
    
    //  Set a Sprite Color
    .pseudocommand @sprite_set_color sprite : color {

        .if (sprite.getType() == AT_IMMEDIATE ) {
            ldx #sprite.getValue()
        } else {
            ldx sprite.getValue()
        }

        .if(color.getType() == AT_IMMEDIATE) {
            lda #color.getValue()
        } else {
            lda color.getValue()
        }

        sta VIC2.SP0COL, x
    }

    //  Set Both Multicolor Registers
    .pseudocommand @sprite_set_multicolor color0 : color1 {

        sprite_set_multicolor0 color0
        sprite_set_multicolor1 color1
    }

    //  Set the First Multicolor Register
    .pseudocommand @sprite_set_multicolor0 color {

        .if(color.getType() == AT_IMMEDIATE) {
            lda #color.getValue()
        } else {
            lda color.getValue()
        }

        sta VIC2.SPMC0
    }

    //  Set the Second Multicolor Register
    .pseudocommand @sprite_set_multicolor1 color {

        .if(color.getType() == AT_IMMEDIATE) {
            lda #color.getValue()
        } else {
            lda color.getValue()
        }

        sta VIC2.SPMC1
    }

    //  Move Sprite to a Position on the Screen
    .pseudocommand @sprite_move_to sprite : xpos : ypos {

        .if(sprite.getType() == AT_IMMEDIATE) {
            ldx #sprite.getValue() * 2
        } else {
            lda sprite.getValue()
            adc sprite.getValue()
            tax
        }

        .if(xpos.getType() == AT_IMMEDIATE) {
            lda #xpos.getValue()
        } else {
            lda xpos.getValue()
        }

        sta VIC2.SP0X, x

        .if(ypos.getType() == AT_IMMEDIATE) {
            lda #ypos.getValue()
        } else {
            lda ypos.getValue()
        }

        sta VIC2.SP0Y, x
    }

    //  Reset Sprite Position
    .pseudocommand @sprite_reset_position sprite {
        sprite_move_to sprite : #$00 : #$00
    }

    .pseudocommand @sprite_set_msb_on sprite {
    }

    .pseudocommand @sprite_set_msb_off sprite {
    }

    //  Reset All Sprite Settings
    .pseudocommand @sprite_reset_all {

        .for(var i = 0; i < 8; i++) {
            .print ("resetingt sprite " + i)
            sprite_reset_position i
            sprite_set_color #i : #VIC2.COLOR_BLACK
        }

        sprite_reset_msb
        sprite_reset_multicolor_mode
        sprite_disable_all
    }

    //  Reset the Sprite MSB Register
    .pseudocommand @sprite_reset_msb {
        lda #$00
        sta VIC2.MSGIX        
    }

    //  Set All Sprites to use Multicolor Mode
    .pseudocommand @sprite_set_multicolor_mode {
        lda #$ff
        sta VIC2.SPMC
    }

    //  Reset Sprite Multicolor Mode Register
    .pseudocommand @sprite_reset_multicolor_mode {
        lda #$00
        sta VIC2.SPMC
    }

    //  Disable All Sprites
    .pseudocommand @sprite_disable_all {
        lda #$00
        sta VIC2.SPENA
    }

    //  Enable All Sprites
    .pseudocommand @sprite_enable_all {
        lda #$ff
        sta VIC2.SPENA
    }
}
